/**
 * Liste des mots à proutifier.
 * Pour en rajouter, veuillez respecter la syntaxe :
 * 
 * "nom à chercher dans la page": {
 *    "regex": "expression rationnelles pour trouver le nom",
 *    "replaceWith": "valeur de remplacement"
 * },
 * 
 * 
 * Référence pour écrire des expressions rationnelles : https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Regular_Expressions
 */
const prouts = {
  "49.3": {
    "regex": "49\\.3",
    "replaceWith": "49.prout"
  },
  "49-3": {
    "regex": "49-3",
    "replaceWith": "49-prout"
  },
  "ministre": {
    "regex": "\\bministre",
    "replaceWith": "miniprout"
  },
  "agnès pannier-runacher": {
    "regex": "agnès pannier-runacher",
    "replaceWith": "agnès proutier-runacher"
  },
  "amélie de montchalin": {
    "regex": "amélie de montchalin",
    "replaceWith": "amélie de proutalin"
  },
  "anne hidalgo": {
    "regex": "anne hidalgo",
    "replaceWith": "anne hidalprout"
  },
  "aurore bergé": {
    "regex": "aurore bergé",
    "replaceWith": "aurore berprout"
  },
  "aurore berge": {
    "regex": "aurore berge",
    "replaceWith": "aurore berprout"
  },
  "bardella": {
    "regex": "bardella",
    "replaceWith": "proutella"
  },
  "barbara pompili": {
    "regex": "barbara pompili",
    "replaceWith": "barbara proutili"
  },
  "bashar al-assad": {
    "regex": "bashar al-assad",
    "replaceWith": "bashar al-asprout"
  },
  "bernard arnault": {
    "regex": "bernard arnault",
    "replaceWith": "petnard arnault"
  },
  "bettencourt": {
    "regex": "bettencourt",
    "replaceWith": "petencourt"
  },
  "bfm": {
    "regex": "bfm",
    "replaceWith": "petfm"
  },
  "bill gates": {
    "regex": "bill gates",
    "replaceWith": "bill pète"
  },
  "boris johnson": {
    "regex": "boris johnson",
    "replaceWith": "beauprout johnson"
  },
  "bruno le maire": {
    "regex": "bruno le maire",
    "replaceWith": "bruno le prout"
  },
  "bruno lemaire": {
    "regex": "bruno lemaire",
    "replaceWith": "bruno le prout"
  },
  "bruno retailleau": {
    "regex": "bruno retailleau",
    "replaceWith": "bruno retailleprout"
  },
  "castaner": {
    "regex": "castaner",
    "replaceWith": "cacastaner"
  },
  "catherine colonna": {
    "regex": "catherine colonna",
    "replaceWith": "catherine colon"
  },
  "catherine vautrin": {
    "regex": "catherine vautrin",
    "replaceWith": "catherine purin"
  },
  "ce que l'on sait": {
    "regex": "ce que l'on sait",
    "replaceWith": "ce que l'on proute"
  },
  "cedric o": {
    "regex": "cedric o",
    "replaceWith": "cédric prout"
  },
  "cédric o": {
    "regex": "cédric o",
    "replaceWith": "cédric prout"
  },
  "chatgpt": {
    "regex": "chatgpt",
    "replaceWith": "chatgprouté"
  },
  "chat gpt": {
    "regex": "chat gpt",
    "replaceWith": "chat gprouté"
  },
  "chirac": {
    "regex": "chirac",
    "replaceWith": "chieprout"
  },
  "christian estrosi": {
    "regex": "christian estrosi",
    "replaceWith": "christian estroprout"
  },
  "christine bouton": {
    "regex": "christine bouton",
    "replaceWith": "christine proutin"
  },
  "christophe barbier": {
    "regex": "christophe barbier",
    "replaceWith": "christophe barpet"
  },
  "christophe béchu": {
    "regex": "christophe béchu",
    "replaceWith": "christophe petchu"
  },
  "confinement": {
    "regex": "confinement",
    "replaceWith": "confiprout"
  },
  "coronavirus": {
    "regex": "coronavirus",
    "replaceWith": "coronaproutus"
  },
  "couac": {
    "regex": "couac",
    "replaceWith": "prout"
  },
  "covid": {
    "regex": "covid",
    "replaceWith": "coprout"
  },
  "cyril hanouna": {
    "regex": "cyril hanouna",
    "replaceWith": "cyril hanounaprout"
  },
  "d'olivier véran": {
    "regex": "d'olivier véran",
    "replaceWith": "de proutivier véran"
  },
  "darmanin": {
    "regex": "darmanin",
    "replaceWith": "darmaprout"
  },
  "dassault": {
    "regex": "dassault",
    "replaceWith": "petault"
  },
  "david rachline": {
    "regex": "david rachline",
    "replaceWith": "david proutline"
  },
  "député": {
    "regex": "député",
    "replaceWith": "déprouté"
  },
  "donald trump": {
    "regex": "donald trump",
    "replaceWith": "donald prout"
  },
  "edwige diaz": {
    "regex": "edwige diaz",
    "replaceWith": "edwige prout"
  },
  "elisabeth borne": {
    "regex": "elisabeth borne",
    "replaceWith": "élisabeth prout"
  },
  "élisabeth borne": {
    "regex": "élisabeth borne",
    "replaceWith": "élisabeth prout"
  },
  "elon musk": {
    "regex": "elon musk",
    "replaceWith": "pet-lon musk"
  },
  "emmanuelle wargon": {
    "regex": "emmanuelle wargon",
    "replaceWith": "emmanuelle prouton"
  },
  "erdogan": {
    "regex": "erdogan",
    "replaceWith": "erdoprout"
  },
  "erdoğan": {
    "regex": "erdoğan",
    "replaceWith": "erdoprout"
  },
  "éric ciotti": {
    "regex": "éric ciotti",
    "replaceWith": "éric proutti"
  },
  "éric dupond-moretti": {
    "regex": "éric dupond-moretti",
    "replaceWith": "éric duprout-moretti"
  },
  "éric lombard": {
    "regex": "éric lombard",
    "replaceWith": "éric lomprout"
  },
  "éric zemmour": {
    "regex": "éric zemmour",
    "replaceWith": "éric zeprout"
  },
  "eric zemmour": {
    "regex": "eric zemmour",
    "replaceWith": "eric zeprout"
  },
  "élections européennes": {
    "regex": "élections européennes",
    "replaceWith": "élections europroutéennes"
  },
  "florence parly": {
    "regex": "florence parly",
    "replaceWith": "florence proutly"
  },
  "florian philippot": {
    "regex": "florian philippot",
    "replaceWith": "floriant philiprout"
  },
  "franck riester": {
    "regex": "franck riester",
    "replaceWith": "franck prouster"
  },
  "françois bayrou": {
    "regex": "françois bayrou",
    "replaceWith": "françois bayprout"
  },
  "françois hollande": {
    "regex": "françois hollande",
    "replaceWith": "françois proutland"
  },
  "francois hollande": {
    "regex": "francois hollande",
    "replaceWith": "francois proutland"
  },
  "frederique vidal": {
    "regex": "frederique vidal",
    "replaceWith": "frédérique proutal"
  },
  "frédérique vidal": {
    "regex": "frédérique vidal",
    "replaceWith": "frédérique proutal"
  },
  "gabriel attal": {
    "regex": "gabriel attal",
    "replaceWith": "gabriel prouttal"
  },
  "gazprom": {
    "regex": "gazprom",
    "replaceWith": "gazprout"
  },
  "geneviève darrieussecq": {
    "regex": "geneviève darrieussecq",
    "replaceWith": "geneviève duproutsecq"
  },
  "geoffroy roux de bézieux": {
    "regex": "geoffroy roux de bézieux",
    "replaceWith": "geoffroy prout de bézieux"
  },
  "gérard larcher": {
    "regex": "gérard larcher",
    "replaceWith": "gérard larchiasse"
  },
  "grégoire de fournas": {
    "regex": "grégoire de fournas",
    "replaceWith": "grégoire de fourprout"
  },
  "hélène laporte": {
    "regex": "hélène laporte",
    "replaceWith": "hélène laprout"
  },
  "intelligence artificielle": {
    "regex": "intelligence artificielle",
    "replaceWith": "imbécilité aprouticielle"
  },
  "j. k. rowling": {
    "regex": "j\\. k\\. rowling",
    "replaceWith": "j. k. rowprout"
  },
  "j k rowling": {
    "regex": "j k rowling",
    "replaceWith": "j k rowprout"
  },
  "jair bolsonaro": {
    "regex": "jair bolsonaro",
    "replaceWith": "jair bolsonaprout"
  },
  "jean castex": {
    "regex": "jean castex",
    "replaceWith": "jean proutex"
  },
  "jean yves le drian": {
    "regex": "jean yves le drian",
    "replaceWith": "jean-prout le drian"
  },
  "jean-baptiste djebbari": {
    "regex": "jean-baptiste djebbari",
    "replaceWith": "jean-baptiste djeprouti"
  },
  "jean-marie le pen": {
    "regex": "jean-marie le pen",
    "replaceWith": "jean-marie le prout"
  },
  "jean-michel blanquer": {
    "regex": "jean-michel blanquer",
    "replaceWith": "jean-michel blanc-prout"
  },
  "jeff bezos": {
    "regex": "jeff bezos",
    "replaceWith": "jeff petzos"
  },
  "johnny deep": {
    "regex": "johnny deep",
    "replaceWith": "johnny pet"
  },
  "jordan bardella": {
    "regex": "jordan bardella",
    "replaceWith": "jordan proutella"
  },
  "julien sanchez": {
    "regex": "julien sanchez",
    "replaceWith": "julien sanprout"
  },
  "la manif pour tous": {
    "regex": "la manif pour tous",
    "replaceWith": "la manif prout tous"
  },
  "le figaro": {
    "regex": "le figaro",
    "replaceWith": "le figarot"
  },
  "léa salamé": {
    "regex": "léa salamé",
    "replaceWith": "léa salapet"
  },
  "les républicains": {
    "regex": "les républicains",
    "replaceWith": "les petplubicains"
  },
  "louis aliot": {
    "regex": "louis aliot",
    "replaceWith": "louis proutiot"
  },
  "macron": {
    "regex": "macron",
    "replaceWith": "maprout"
  },
  "manuel valls": {
    "regex": "manuel valls(?!-vendre)",
    "replaceWith": "manuel valls-vendre-partout"
  },
  "marc fesneau": {
    "regex": "marc fesneau",
    "replaceWith": "marc faitprout"
  },
  "marine le pen": {
    "regex": "marine le pen",
    "replaceWith": "marine le prout"
  },
  "marlène schiappa": {
    "regex": "marlène schiappa",
    "replaceWith": "marlène schiaprout"
  },
  "martine aubry": {
    "regex": "martine aubry",
    "replaceWith": "martine auproute"
  },
  "mélenchon": {
    "regex": "mélenchon",
    "replaceWith": "mélenprout"
  },
  "michel barnier": {
    "regex": "michel barnier",
    "replaceWith": "michel barpet"
  },
  "mitterrand": {
    "regex": "mitterrand",
    "replaceWith": "mitteprout"
  },
  "nicolas sarkozy": {
    "regex": "nicolas sarkozy",
    "replaceWith": "nicoprout sarkozy"
  },
  "olivia grégroire": {
    "regex": "olivia grégroire",
    "replaceWith": "olivia petgoire"
  },
  "olivier dussopt": {
    "regex": "olivier dussopt",
    "replaceWith": "olivier duprout"
  },
  "olivier véran": {
    "regex": "olivier véran",
    "replaceWith": "olivier pétant"
  },
  "pap ndiaye": {
    "regex": "pap ndiaye",
    "replaceWith": "prout ndiaye"
  },
  "pascal praud": {
    "regex": "pascal praud",
    "replaceWith": "pascal prout"
  },
  "patrick drahi": {
    "regex": "patrick drahi",
    "replaceWith": "proutick drahi"
  },
  "patrick pouyanné": {
    "regex": "patrick pouyanné",
    "replaceWith": "patrick proutanné"
  },
  "plan de relance": {
    "regex": "plan de relance",
    "replaceWith": "prout de relance"
  },
  "poutine": {
    "regex": "poutine",
    "replaceWith": "proutine"
  },
  "président": {
    "regex": "président",
    "replaceWith": "proutident"
  },
  "protocole sanitaire": {
    "regex": "protocole sanitaire",
    "replaceWith": "proutocole sanitaire"
  },
  "matignon": {
    "regex": "matignon",
    "replaceWith": "proutignon"
  },
  "rachida dati": {
    "regex": "rachida dati",
    "replaceWith": "rachida daprout"
  },
  "rassemblement national": {
    "regex": "rassemblement national",
    "replaceWith": "prout-sent-vraiment national"
  },
  "roselyne bachelot": {
    "regex": "roselyne bachelot",
    "replaceWith": "proutelyne bachelot"
  },
  "sébastien chenu": {
    "regex": "sébastien chenu",
    "replaceWith": "sébastien cheprout"
  },
  "sébastion lecornu": {
    "regex": "sébastien lecornu",
    "replaceWith": "sébastien leproutu"
  },
  "sénateur": {
    "regex": "sénateur",
    "replaceWith": "sénaprout"
  },
  "sergueï lavrov": {
    "regex": "sergueï lavrov",
    "replaceWith": "sergueï lavprout"
  },
  "sciences po": {
    "regex": "sciences po",
    "replaceWith": "sciences prout"
  },
  "stanislas guerini": {
    "regex": "stanislas guerini",
    "replaceWith": "stanislas proutini"
  },
  "sylvie retaileau": {
    "regex": "sylvie retaileau",
    "replaceWith": "sylvie retailleprout"
  },
  "vincent bolloré": {
    "regex": "vincent bolloré",
    "replaceWith": "vincent bolloprout"
  },
  "xavier niel": {
    "regex": "xavier niel",
    "replaceWith": "xavier prout"
  },
  "yaël braun-pivet": {
    "regex": "yaël braun-pivet",
    "replaceWith": "yaël prout-pivet"
  },
  "yael braun-pivet": {
    "regex": "yael braun-pivet",
    "replaceWith": "yael prout-pivet"
  },
}

export default prouts;
